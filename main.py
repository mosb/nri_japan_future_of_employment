# Copyright (c) 2015 Michael Osborne, mosb@robots.ox.ac.uk

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# automatically reload all modules
import sys
if globals().has_key('init_modules'):
    for m in [x for x in sys.modules.keys() if x not in init_modules]:
        del(sys.modules[m])
else:
    init_modules = sys.modules.keys()

import load_data
import classification
import csv

Japan_file = "automation_japan_training_data.xls"

dfs = load_data.load_data(Japan_file=Japan_file)
dfs['train'].to_excel('Japan_training_df.xls')

train = load_data.prep_data_for_classification(dfs['train'])

test = load_data.prep_data_for_classification(dfs['test'])

X = train['features']
y = train['labels']
feature_names = train['feature_names']
X_test = test['features']

post_feature_selection = classification.select_features(
    X, y, feature_names, X_test
)

X = post_feature_selection['features']
X_test = post_feature_selection['features_test']
feature_names = post_feature_selection['feature_names']

final = classification.run_cross_validations_and_output_final_classification(
    X, y, X_test, feature_names
)
dfs['test']['Probabilities of Automation'] = final[
    'automatable_probabilities'
]
dfs['test'].to_excel('automation_japan.xls', index=False)

# write an output file with kernels in it
file = open("kernels.csv", "wb")
w = csv.writer(file)
w.writerow(['kernel', 'AUC', 'logL'])
for (ind_kernel, kernel) in enumerate(final['results']['kernels']):
    w.writerow(
        [
            kernel.name,
            final['results']['score_AUC'][ind_kernel],
            final['results']['score_logL'][ind_kernel]
        ]
    )
file.close()

# write an output file with feature scores in it
final['feature_importance'].to_csv('features.csv')
