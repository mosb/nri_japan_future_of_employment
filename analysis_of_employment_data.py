# Copyright (c) 2014 Michael Osborne, mosb@robots.ox.ac.uk

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# automatically reload all modules
import sys
if globals().has_key('init_modules'):
    for m in [x for x in sys.modules.keys() if x not in init_modules]:
        del(sys.modules[m])
else:
    init_modules = sys.modules.keys()

import pandas as pd
import automation_plots

japan_data = pd.read_excel(
    '20151222_ProbabilitiesOfAutomation_JP_Datasets_v0.92.xlsx',
    sheetname='Dataset1(601Jobtitles)',
    header=3,
    na_values=['-']
)
prob_col = 'Probabilities of automation(%)'
probability_thresholds = [0, 0.3, 0.7, 1]
risk_names = ['1. Low', '2. Medium', '3. High']

weights_col = 'Number of employed persons (Perosons)'

country_summary = automation_plots.stacked_automation_plot(
    japan_data,
    probability_column=prob_col,
    employment_column=weights_col
)
print(country_summary['High probability of automation percent'])

def assign_risk_name(probability):
    for i_prob in range(len(probability_thresholds)-1):
        lo_prob = probability_thresholds[i_prob]
        hi_prob = probability_thresholds[i_prob + 1]

        if (probability >= lo_prob) & (probability < hi_prob):
            return risk_names[i_prob]

risk_col = 'Automatability risk'

japan_data[risk_col] = japan_data[prob_col].map(assign_risk_name)

japan_grouped = japan_data.groupby(risk_col)

japan_summary = japan_grouped.describe().T

for i_prob in range(len(probability_thresholds)-1):
    risk_name = risk_names[i_prob]
    japan_summary[risk_name, 'sum'] = (
        japan_summary[risk_name, 'count']
         *
        japan_summary[risk_name, 'mean']
    )

japan_summary.sortlevel(0, axis=1, inplace=True)
japan_summary.T.to_excel('japan_summary.xls')


# Code below from http://stackoverflow.com/questions/10951341/pandas-dataframe-aggregate-function-using-multiple-columns
def wavg_func(data_col, weights_col):
    def wavg(group):
        dd = group[data_col]
        ww = group[weights_col] * 1.0
        return (dd * ww).sum() / ww.sum()
    return wavg


japan_weighted_summary = japan_grouped.agg({weights_col:sum})
data_cols = [
    u'Annual income\n(10,000 yen)',
    u'Ratio of temporary staffs (%)',
    u'Jobs to applicants ratio(%)',
    u'Probabilities of automation(%)'
]
for dcol in data_cols:
    wavg_f = wavg_func(dcol, weights_col)
    japan_weighted_summary[dcol] = japan_grouped.apply(wavg_f)

japan_weighted_summary.to_excel('japan_weighted_summary.xls')
