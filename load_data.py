# Copyright (c) 2015 Michael Osborne, mosb@robots.ox.ac.uk

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import pandas as pd
import numpy as np

def load_data(Japan_file):

    """
    read in automation data from excel spreadsheets.

    Parameters
    ----------
    Japan_file : str
        filename containing all data on Japanese occupations, including
        training set.

    Returns
    -------
    out : dict
        contains keys 'train' and 'test', both describing dataframes
    """

    japan_df = pd.read_excel(
        Japan_file,
        sheetname='automation_data'
    )
    japan_df['No.'].map(int)
    japan_df = japan_df.set_index('No.', drop=False)

    japan_train_df = japan_df.dropna(subset=['Automatable Training Label'])

    out = dict(
        train=japan_train_df,
        test=japan_df
    )
    return out

def prep_data_for_classification(df):

    """
    takes a dataframe and turns into a pair of arrays (N by D and N by 1),
    ready for classification to take place.

    Parameters
    ----------
    df : DataFrame
        Has columns 'Job titles' and 'Automatable Training Label'

    Returns
    -------
    out : dict
        has keys:
        'features' (N by D array);
        'labels' (N by 1 array);
        'feature_names' (length N Series describing features);
        'instance_names' (N by 1 Dataframe describing job codes and their
        names).

    """

    # features, by definition in the spreadsheet, include prefix brackets like
    # this: '[Working Environment] Outdoor (Work Setting)'

    instance_names = df['Job titles']

    X_df = df.filter(regex='\[.+\]', axis=1)
    feature_names = X_df.columns.values
    X = X_df.values
    y = df['Automatable Training Label'].values[:, None]

    return dict(
        features=X,
        labels=y,
        feature_names=feature_names,
        instance_names=instance_names
    )


    def load_automatable_label_data(US_training_file, Japan_file):

        """
        read in automatable label data from excel spreadsheets. This function is
        now deprecated; being useful only for the original data supplied.

        Parameters
        ----------
        US_training_file : str
            filename of data describing the training set from the US along with
            mappings to Japanese occupations
        Japan_file : str
            filename containing all data on Japanese occupations

        Returns
        -------
        out : dict
            contains keys 'train' and 'test', both describing dataframes
        """

        japan_df = pd.read_excel(
            Japan_file,
            sheetname='Original Data',
            skip_footer=1
        )
        japan_df['No.'].map(int)
        japan_df = japan_df.set_index('No.', drop=False)
        japan_df['Automatable Training Label'] = np.nan

        US_train_df = pd.read_excel(
            US_training_file,
            sheetname='US&Japan Training Set'
        )

        ix_US_train = US_train_df['Training set automatable labels'].isin([1, 0])
        US_train_df = US_train_df[ix_US_train]
        n_train = US_train_df.shape[0]

        # Initialise to empty dataframe mimicking japan_df
        japan_train_df = japan_df.iloc[[]]

        for i in xrange(n_train):
            occupation = US_train_df.iloc[i]
            japan_occupation_code_str = occupation['Japan(No.)']

            if isinstance(japan_occupation_code_str, unicode):
                # japan_occupation_code is a unicode list that
                # contains multiple codes
                japan_occupation_code = map(
                    int,
                    japan_occupation_code_str.split('&')
                )
            elif (
                isinstance(japan_occupation_code_str, float)
                and
                np.isnan(japan_occupation_code_str)
            ):
                # ignore this occupation: no match in japanese occupations
                # could be found
                continue
            else:
                # If neither of the conditions above match,
                # japan_occupation_code is an in
                japan_occupation_code = japan_occupation_code_str

            japan_df.ix[
                japan_occupation_code, 'Automatable Training Label'
            ] = occupation[
                'Training set automatable labels'
            ]
            new_train_rows = japan_df.ix[japan_occupation_code].copy()

            japan_train_df = japan_train_df.append(new_train_rows)

        out = dict(
            train=japan_train_df,
            test=japan_df
        )
        return out
