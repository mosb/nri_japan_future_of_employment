import numpy as np
import GPy
import pandas as pd
# import savefig
import pylab as pb

def log_likelihood(y_test, probs):
    # evaluate posterior predictive likelihood
    return np.sum(np.log((1 - probs) * (1 - y_test) + (probs) * y_test))

def classification(X_train, y_train, kernel,
                   X_test=np.empty([0, 1]), optimize=True, max_iters=5000):

    # Create GP model
    m = GPy.models.GPClassification(
        X_train,
        y_train,
        # likelihood=GPy.likelihoods.link_functions.Heaviside(),
        kernel=kernel
    )

    # Assumes:
    # * Probit link function
    # * Bernoulli noise
    # * EP inference

    # Optimize
    if optimize:

        print('optimizing ...')
        m.optimize(max_f_eval=max_iters, messages=True)

    if X_test.size != 0:
        # Test
        probs = m.predict(X_test)[0]
    else:
        probs = np.empty([0, 1])

    return dict(probs=probs, model=m)


def create_kernels(
    X_train,
    y_train,
    kernel_templates=[
        GPy.kern.RBF,
        GPy.kern.Linear,
        GPy.kern.Matern32
    ]
):
    """
    Create a set of kernels using training data
    """

    num_dims = X_train.shape[1]
    kernels = []

    for kernel_template in kernel_templates:

        args = {}
        if not 'linear' in str(kernel_template):
            # This kernel has lengthscales
            args = dict(ARD=True, lengthscale=1.)

        kernel = kernel_template(num_dims, **args)

        kernels.append(kernel)

    return kernels

def select_features(
    X_all,
    y,
    feature_names,
    X_test=np.empty([0, 1]),
    kernel_templates=[GPy.kern.RBF],
    optimize=True,
    max_iters=1000,
    n_selected_features=10
):
    """
    Perform greedy forward selection feature with log_likelihood as score.

    Parameters
    ----------


    Returns
    -------

    """

    X_left = X_all
    feature_names_left = feature_names
    n_features_left = X_left.shape[1]
    X_selected = X_all[:, []]
    feature_names_selected = feature_names[[]]
    scores_selected = np.empty(n_selected_features)

    X_test_supplied = X_test.size != 0
    if X_test_supplied:
        X_test_selected = X_test[:, []]
        X_test_left = X_test
    else:
        X_test_selected = np.empty([0, 1])

    for i_selection in xrange(n_selected_features):

        score = np.zeros(n_features_left)

        for i_trial in xrange(n_features_left):

            X_trial = np.hstack((X_selected, X_left[:, i_trial][:, None]))
            kernels = create_kernels(X_trial, y, kernel_templates)
            out = classification(X_trial, y, kernels[0])
            score[i_trial] = out['model'].log_likelihood()

        i_best_trial = score.argmax()
        scores_selected[i_selection] = score[i_best_trial]

        # add feature to those currently selected

        X_selected = np.hstack((X_selected, X_left[:, i_best_trial][:, None]))
        feature_names_selected = np.hstack((
            feature_names_selected,
            feature_names_left[[i_best_trial]]
        ))

        # remove feature from those remaining

        X_left = np.delete(X_left, i_best_trial, 1)
        feature_names_left = np.delete(feature_names_left, i_best_trial, 0)
        n_features_left = X_left.shape[1]

        if X_test_supplied:
            X_test_selected = np.hstack(
                (X_test_selected, X_test_left[:, i_best_trial][:, None])
            )
            X_test_left = np.delete(X_test_left, i_best_trial, 1)

    return dict(
        features=X_selected,
        features_test=X_test_selected,
        feature_names=feature_names_selected,
        scores=scores_selected
    )


def cross_validate(X, y, kernels):

    from sklearn.metrics import roc_auc_score
    from sklearn.cross_validation import StratifiedKFold

    num_kernels = len(kernels)
    num_folds = 10
    # This means that (num_folds - 1)/num_folds of the data is used to train,
    # and 1/num_folds of the data is used to test.

    kf = StratifiedKFold(y.flatten(), n_folds=num_folds)

    score_AUC = np.zeros((num_folds, num_kernels))
    score_logL = score_AUC.copy()

    # models will store the GP models in a nested list; will have to be built
    # up incrementally
    models = []
    for ind_fold, (train, test) in enumerate(kf):
        X_train, X_test, y_train, y_test = (
            X[train], X[test], y[train], y[test]
        )

        models.append([])
        print "Performing fold " + str(ind_fold) + "\n=================\n"

        for ind_kernel, kernel in enumerate(kernels):

            out = classification(X_train, y_train, kernel, X_test)

            print ("Using model: " + str(ind_kernel) +
                    "\n" + str(out['model']) + "\n")

            probs = out['probs']

            score_AUC[ind_fold, ind_kernel] = roc_auc_score(y_test, probs)
            score_logL[ind_fold, ind_kernel] = log_likelihood(y_test, probs)
            models[ind_fold].append(out['model'])

    score_AUC = np.mean(score_AUC, 0)
    score_logL = np.sum(score_logL, 0)

    return dict(
        score_AUC=score_AUC,
        score_logL=score_logL,
        kernels=kernels,
        models=models
    )


def run_cross_validations_and_output_final_classification(
        X_train,
        y_train,
        X_test,
        feature_names,
        plot=False
    ):

    # X_train = X_train.values
    # y_train = y_train.values[:, None]
    # X_test = X_test.values

    kernels = create_kernels(X_train, y_train)

    results = cross_validate(X_train, y_train, kernels)

    if plot:

        num_features = X_train.shape[1]
        ind_test_features = range(num_features)
        models = results['models']

        for (ind_feature, feature) in enumerate(feature_names):

            ind_fixed_features = ind_test_features[:]
            ind_fixed_features.remove(ind_feature)
            fixed_inputs = [(i, 50) for i in ind_fixed_features]

            for (ind_kernel, kernel) in enumerate(kernels):

                for ind_fold in range(len(models)):

                    m = models[ind_fold][ind_kernel]

                    m.plot(fixed_inputs=fixed_inputs)
                    # savefig.save(
                    #     'figs/'
                    #     + str(feature).replace(' ', '_') + '_'
                    #     + str(kernel).split('.')[0].strip() + '_'
                    #     + str(ind_fold),
                    #     ext='png',
                    #     close='True',
                    #     verbose='True'
                    # )

    best_kernel_ind = results['score_logL'].argmax()
    best_kernel = kernels[best_kernel_ind]

    classification_outputs = classification(
        X_train,
        y_train,
        best_kernel,
        X_test,
        optimize=True,
        max_iters=1000
    )
    automatable_probabilities = classification_outputs['probs']
    model = classification_outputs['model']

    if 'linear' not in str(best_kernel):
        lengthscales = np.array(model.parameters[1].lengthscale)

        feature_importance = pd.Series(
            index=feature_names, data=1/lengthscales
        )
        feature_importance.sort(inplace=True)
    else:
        feature_importance = {}

    return dict(
        results=results,
        automatable_probabilities=automatable_probabilities,
        model=model,
        feature_importance=feature_importance
    )

