Python code to perform the classification of Japanese occupations as being automatable or not. Performed for Nomura Research Institute. 

* `main.py` runs the classification algorithm.
* `analysis_of_employment_data.py` follows up by making some plots.

Mike Osborne
mosb@robots.ox.ac.uk, 2015-07-15