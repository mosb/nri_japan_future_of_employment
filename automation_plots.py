# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import itertools
import matplotlib.patches as mpatches
import os
dir = os.path.split(os.path.realpath(__file__))[0]
figures_dir = dir + '/figures/'

# Seaborn defaults
sns.set_palette('Paired', 10)
sns.set_style("dark")

probability_thresholds = [0, 0.3, 0.7, 1]
risk_names = ['Low', 'Med', 'High']

percent_risk_names = [
    r + ' probability of automation percent'
    for r in risk_names
]

def stacked_automation_plot(
    country_data,
    categories=pd.DataFrame(
        {'category_number': 0, 'category_name': 'default'}, index=[0]
    ),
    probability_column='Probability of automation',
    employment_column='# of employed individuals',
    category_number_col='category_number',
    category_column='category'
):
    """
    Make stacked plot of the automatability of a country's employment

    Parameters
    ----------
    country_data : DataFrame
    categories : DataFrame

    Returns
    -------
    country_summary : DataFrame

    """

    # Define units of employment (e.g. thousands or millions)
    units = 1e6
    units_name = '(M)'

    num_prob_bins = 1000

    prob_bins = np.linspace(0, 1, num_prob_bins)
    window_size = round(num_prob_bins/10)

    num_categories = len(categories)
    is_categorised = num_categories > 1

    employment = np.empty([num_categories, num_prob_bins])

    for category_ix, category in enumerate(
        categories[category_number_col].values
    ):

        for prob_ix in range(num_prob_bins):

            if is_categorised:
                category_mask = (country_data[category_column] == category)
            else:
                # a hack to select all data
                category_mask = (country_data[probability_column] >= 0)

            category_probs = (
                country_data[probability_column][category_mask]
            )

            category_employment = (
                country_data[employment_column][category_mask]
                /
                units
            )

            lower_lim = max(0, prob_ix - window_size)
            upper_lim = min(num_prob_bins - 1, prob_ix + window_size)

            in_bin_mask = (
                (category_probs > prob_bins[lower_lim])
                &
                (category_probs < prob_bins[upper_lim])
            )

            employment[category_ix, prob_ix] = (
                sum(category_employment[in_bin_mask])
                /
                ((upper_lim - lower_lim) / num_prob_bins)
            )

    plt.figure()
    ax = plt.gca()
    plt.stackplot(prob_bins, employment)
    plt.ylabel('Employment ' + units_name)
    plt.xlabel('Probability of Automation')

    country_summary = find_high_med_low_probability_fractions(
        country_data,
        probability_column=probability_column,
        employment_column=employment_column
    )

    for i_prob in range(len(probability_thresholds)-1):

        field = percent_risk_names[i_prob]

        descriptive_text = (
            u"←  " + risk_names[i_prob] + u"  →\n"
            + "{0:.0f}".format(country_summary[field].values[0])
            + "% Employment"
        )

        plt.text(
            np.mean(probability_thresholds[i_prob:i_prob+2]),
            0.95,
            descriptive_text,
            transform=ax.transAxes,
            horizontalalignment='center',
            verticalalignment='top',
            bbox={'facecolor': 'white', 'edgecolor': 'none', 'pad': 10}
        )

        x_threshold = probability_thresholds[i_prob+1]
        plt.plot(
            [x_threshold, x_threshold],
            [0, 1 * ax.get_ylim()[1]],
            '-',
            linewidth=1,
            color='w'
        )


    plt.savefig(figures_dir + 'automation_plot.png')

    plt.close()

    return country_summary


def make_legend(categories, category_name_col='category_number'):

    palette = itertools.cycle(sns.color_palette())

    category_names = categories[category_name_col].values.tolist();

    plt.figure()
    plt.legend(
        [
            mpatches.Patch(color=next(palette))
            for i in range(len(category_names))
        ],
        category_names
    )
    plt.savefig(figures_dir + 'legend.png')

def find_high_med_low_probability_fractions(
    country_data,
    probability_column='Probability of automation',
    employment_column='# of employed individuals'
):

    """
    Compute the fractions of this country's employment in high, medium and low
    probability of automation categories.

    Parameters
    ----------
    country_data : DataFrame


    Returns
    -------
    country_summary : DataFrame

    """

    employment_risk_names = [
        r + ' probability of automation employment'
        for r in risk_names
    ]


    country_total_employment = country_data[employment_column].sum()

    # Initialise empty-ish DataFrame
    risk_columns = percent_risk_names + employment_risk_names
    country_summary_content = dict({
        s: np.nan
        for s in risk_columns
    })
    country_summary_content['total_employment'] = country_total_employment
    country_summary = pd.DataFrame(
        country_summary_content,
        index=[0]
    )

    for i_prob in range(len(probability_thresholds)-1):
        lo_prob = probability_thresholds[i_prob]
        hi_prob = probability_thresholds[i_prob + 1]
        in_range_data = country_data.ix[
            (country_data[probability_column] > lo_prob)
            &
            (country_data[probability_column] < hi_prob)
        ]

        in_range_employment = in_range_data[
            employment_column
        ].sum()
        country_summary[
            employment_risk_names[i_prob]
        ] = in_range_employment
        country_summary[
            percent_risk_names[i_prob]
        ] = 100 * in_range_employment / country_total_employment

    # reorder columns
    columns = risk_columns + ['total_employment']
    country_summary = country_summary[columns]


    return country_summary
